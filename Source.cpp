#include <Windows.h>
#include <ctime>
#include "resource.h"
#define MI_TIMER_INVENTADO 117

time_t actualTime;
tm* timeInfo;

BOOL CALLBACK fVentana1(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrev, PSTR cmdLine, int cShow) {
	HWND hVentana1 = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOG1), NULL, fVentana1);
	ShowWindow(hVentana1, cShow);

	SetTimer(hVentana1,MI_TIMER_INVENTADO,1000,(TIMERPROC)NULL);

	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	while (GetMessage(&msg, NULL, NULL, NULL)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}


BOOL CALLBACK fVentana1(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) {
	switch (msg) {
	case WM_TIMER:{
		time(&actualTime);
		timeInfo = localtime(&actualTime);
		char bufferTime[80];
		strftime(bufferTime,80,"%d-%m-%Y %I:%M:%S", timeInfo);
		SetWindowText(GetDlgItem(hwnd,LBL_TIMER),bufferTime);
	}break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(117);
		break;
	}
	return FALSE;
}